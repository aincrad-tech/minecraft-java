package minecraft;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    Socket server = null;

    public void connect() {
        Scanner keyboard = new Scanner(System.in);
        try {
            server = new Socket(InetAddress.getLocalHost(), 100);
            while (true) {
                System.out.println(this.readSocket());
                String payload = keyboard.nextLine();
                this.writeSocket(payload);
            }
        }
        catch (IOException e) {
        }
        keyboard.close();
    }
    
    public String readSocket(){
        try {
            int dim_buffer = 1000;
            byte[] buffer = new byte[dim_buffer];
            InputStream in = server.getInputStream();
            int letti = in.read(buffer, 0, dim_buffer);
            return new String(buffer, 0, letti);
        } catch (Exception e) {
            System.out.println("Error in reading phase");
            return "";
        }
    }

    public void writeSocket(String payload){
        try {
            OutputStream out = server.getOutputStream();
            out.write(payload.getBytes(), 0, payload.length());
        } catch (IOException e) {
            System.out.println("Error in writing phase");
        }
    }

    public static void main(String[] args) {
        Client c = new Client();
        c.connect();
    }
}
