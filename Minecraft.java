package minecraft;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

public class Minecraft {
    ServerSocket server = null;
    Socket request = null;
    String username;
    public Vector<World> worlds = new Vector<World>();

    public void listen() {
        try {
            server = new ServerSocket(100);
        } catch (IOException e2) {
            System.out.println("Server closed");
        }
        try {
            request = server.accept();

            this.login();

        } catch (Exception e) {
            System.out.println("Error, Closing connection");
            try {
                request.close();
                this.listen();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void login() throws Exception {
        this.writeSocket("Welcome to Minecraft! Please identify yourself");
        String name = this.readSocket();
        this.username = name;
        this.launch();
    }

    public void launch () throws Exception {
        this.writeSocket("Hi " + this.username + ", Choose one of the following actions:\nCreate world\nYour worlds");
        this.menu();
    }

    public void menu() throws Exception {
        while (true) {
            String opt = this.readSocket();

            switch (opt) {
                case "Create world":
                    this.createWorld();
                    break;
                case "Your worlds":
                    this.yourWordls();
                    break;
                default:
                    this.writeSocket("Sorry you insert an invalid command!\n Choose one of the following actions:\nCreate world\nYour worlds");
                    break;
            }
        }
    }

    public void createWorld() throws Exception {
        String name = "";
        while(true){
            this.writeSocket("Insert the name of the world");
            name = this.readSocket();
            if(name.length() > 0) break;
            else this.writeSocket("Empty string is not a valid name");
        }
        World x = new World(name, this.username);
        System.out.println(x.toString());
        this.worlds.add(x);
        this.writeSocket("You world has been created!\n\nPlease chose a new option: \nCreate world\nYour worlds");
    }

    public void yourWordls(){
        String res = "Your worlds are:\n";
        for (World world : worlds) {
            if(world.master.equals(this.username)) res = res.concat(world.name) + ", ";
        }
        this.writeSocket(res.substring(0, res.length() - 2) + "\n\nPlease chose a new option: \nCreate world\nYour worlds");
    }

    public String readSocket() throws Exception {
        int dim_buffer = 1000;
        byte[] buffer = new byte[dim_buffer];
        InputStream in = request.getInputStream();
        int letti = in.read(buffer, 0, dim_buffer);
        return new String(buffer, 0, letti);
        
    }

    public void writeSocket(String payload){
        try {
            OutputStream out = request.getOutputStream();
            out.write(payload.getBytes(), 0, payload.length());
        } catch (IOException e) {
            System.out.println("Error in writing phase");
        }
    }

    public static void main(String[] args) {
        Minecraft m = new Minecraft();
        m.listen();
    }
}
