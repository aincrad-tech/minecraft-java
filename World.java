package minecraft;
import java.util.Vector;

public class World {
    public String master;
    public String name;
    private Vector<String> citizens;

    public World(String name, String master) {
        this.name = name;
        this.master = master;
        this.citizens = new Vector<String>();
        this.citizens.add(master);
    }

    @Override
    public String toString() {
        return "This is " + this.name + " a world created by " + this.master + " which is populated by " + this.citizens.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(this.getClass().isInstance(obj)){
            World cast = (World) obj;
            return cast.name == this.name;
        }
        return false;
    }

    public boolean addUser(String s){
        this.citizens.add(s);
        return true;
    }
}
